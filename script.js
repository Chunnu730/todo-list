const input_field = document.getElementById("input-field");
const btn = document.getElementById("btn");

btn.addEventListener("click", function () {
  const getTodoValue = localStorage.getItem("todoItems");
  if (getTodoValue != null) {
    todoArray = JSON.parse(getTodoValue);
  } else {
    todoArray = [];
  }
  let todoObj = {
    id: Math.trunc(Math.random() * 1000),
    todo: input_field.value,
    checked: false,
  };
  todoArray.push(todoObj);
  localStorage.setItem("todoItems", JSON.stringify(todoArray));
  document.getElementById("input-field").value = "";
  showList();
});

function showList() {
  const getTodoValue = localStorage.getItem("todoItems");
  let str = "";
  if (getTodoValue != null) {
    todoArray = JSON.parse(getTodoValue);
  } else {
    todoArray = [];
  }
  todoArray.forEach((element, index) => {
    str += `<li>
   <label><input type='checkbox' id='checkbox' class='${element.id}'  onclick='checkedItem(${index})'}>
    <span id='${element.id}'>Pending</span><label>
    <span id='todo-content'>${element.todo}</span>
    <button onclick='editItem(${index})' id='edit'>Edit</button>
    <button onclick='deleteItem(${index})' id='delete'>Delete</button>
    </li>`;
  });
  document.getElementById("list-item").innerHTML = str;
  todoArray.forEach((element)=>{
    if(element['checked'] === "true"){
      document.getElementById(element.id).setAttribute('class','flag1');
      document.getElementById(element.id).innerText = 'Done';
      
    }else{
      document.getElementById(element.id).setAttribute('class','flag');
    }
  })
  
}
showList();

function deleteItem(index) {
  todoArray.splice(index, 1);
  localStorage.setItem("todoItems", JSON.stringify(todoArray));
  showList();
}

function editItem(index) {
  document.getElementById("container-1").style.opacity = 0.1;
  document.getElementById("content").style.opacity = 0.1;
  document.getElementById("popup").style.opacity = 1;
  document.getElementById("popup").style.display = "block";

  const getTodo = JSON.parse(localStorage.getItem("todoItems"));
  document.getElementById("popup-input").value = getTodo[index].todo;
  document.getElementById("save").addEventListener("click", function () {
    const getValue = document.getElementById("popup-input").value;
    getTodo[index].todo = getValue;
    localStorage.setItem("todoItems", JSON.stringify(getTodo));
    document.getElementById("container-1").style.opacity = 1;
    document.getElementById("content").style.opacity = 1;
    document.getElementById("popup").style.display = "none";
    showList();
  });
}

function checkedItem(index) {
  const getTodo = JSON.parse(localStorage.getItem("todoItems"));
  if (getTodo[index]["checked"] === "false") {
    getTodo[index]["checked"] = "true";
  } else {
    getTodo[index]["checked"] = "false";
  }
  localStorage.setItem("todoItems", JSON.stringify(getTodo));
  showList();
}